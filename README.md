equilibrator-cache
==================

[![PyPI version](https://badge.fury.io/py/equilibrator-cache.svg)](https://badge.fury.io/py/equilibrator-cache)
[![Anaconda-Server Badge](https://anaconda.org/conda-forge/equilibrator-cache/badges/version.svg)](https://anaconda.org/conda-forge/equilibrator-cache)
[![Python version](https://img.shields.io/pypi/pyversions/equilibrator-cache.svg)](https://www.python.org/downloads)
[![MIT license](https://img.shields.io/pypi/l/equilibrator-cache.svg)](https://mit-license.org/)

[![pipeline status](https://gitlab.com/elad.noor/equilibrator-cache/badges/develop/pipeline.svg)](https://gitlab.com/elad.noor/equilibrator-cache/commits/develop)
[![codecov](https://codecov.io/gl/equilibrator/equilibrator-cache/branch/develop/graph/badge.svg?token=OxxaCqgaLs)](https://codecov.io/gl/equilibrator/equilibrator-cache)
[![Join our Google group](https://img.shields.io/badge/google_group-equilibrator_users-blue)](https://groups.google.com/g/equilibrator-users)
[![Documentation Status](https://readthedocs.org/projects/equilibrator/badge/?version=latest)](https://equilibrator.readthedocs.io/en/latest/?badge=latest)

A database application for caching data used by eQuilibrator and related projects.
Stored data includes compound names, structures, protonation state information,
reaction and enzyme info, and cross-references to other databases.
All compounds stored in equilibrator-cache are cross-referenced using
[InChIKey](https://en.wikipedia.org/wiki/International_Chemical_Identifier#InChIKey).

## Supported Chemical Databases:

* [MetaNetX](https://www.metanetx.org/)
* [KEGG](https://www.kegg.jp/)
* [ChEBI](https://www.ebi.ac.uk/chebi/)

