# The MIT License (MIT)
#
# Copyright (c) 2018, 2019 Institute for Molecular Systems Biology, ETH Zurich.
# Copyright (c) 2018, 2019 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


"""Test the compatibility between annotations."""


from unittest.mock import Mock

import pytest

from equilibrator_cache import compatibility as compat


@pytest.mark.parametrize(
    "metabolites, order, expected",
    [
        ([], (), ()),
        (
            [
                Mock(spec_set=("id", "annotation"), id="atp_c", annotation={}),
                Mock(spec_set=("id", "annotation"), id="adp_c", annotation={}),
            ],
            ("inchikey",),
            (0,),
        ),
        (
            [
                Mock(
                    spec_set=("id", "annotation"),
                    id="atp_c",
                    annotation={"inchikey": ["foo", "goo"]},
                ),
                Mock(
                    spec_set=("id", "annotation"),
                    id="adp_c",
                    annotation={"inchikey": "bar"},
                ),
            ],
            ("inchikey",),
            (2,),
        ),
        (
            [
                Mock(
                    spec_set=("id", "annotation"),
                    id="atp_c",
                    annotation={"inchikey": "foo", "hmdb": "foo"},
                ),
                Mock(
                    spec_set=("id", "annotation"),
                    id="adp_c",
                    annotation={"hmdb": "bar", "other": "bar"},
                ),
            ],
            ("inchikey", "hmdb"),
            (1, 1),
        ),
    ],
)
def test_group_metabolites_by_annotation(metabolites, order, expected):
    result = compat.group_metabolites_by_annotation(metabolites, order)
    for key, num in zip(order, expected):
        assert len(result.get(key, ())) == num
