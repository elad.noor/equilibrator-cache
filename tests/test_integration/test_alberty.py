# The MIT License (MIT)
#
# Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich.
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import pandas as pd
import pytest

from equilibrator_cache import (
    Q_,
    Compound,
    CompoundMicrospecies,
    R,
    Reaction,
    default_T,
)
from equilibrator_cache.thermodynamic_constants import debye_hueckel


@pytest.mark.parametrize(
    ("ionic_strength", "temperature", "expected"),
    [(0.10, 298.15, 0.612), (0.25, 298.15, 0.810), (0.50, 298.15, 0.967)],
)
def test_thermodynamic_constants(ionic_strength, temperature, expected):
    dh = debye_hueckel(ionic_strength**0.5, temperature)
    assert dh == pytest.approx(expected, abs=1e-3)


@pytest.fixture(scope="module")
def microspecies_df() -> pd.DataFrame:
    """Create a DataFrame with example microspecies from Alberty's book."""
    return pd.DataFrame(
        columns=["reactant", "standard_dgf", "z", "n_h", "n_mg"],
        data=[
            ("atp", -2768.10, -4, 12, 0),
            ("atp", -2811.48, -3, 13, 0),
            ("atp", -2838.18, -2, 14, 0),
            ("atp", -3258.68, -2, 12, 1),
            ("atp", -3287.50, -1, 13, 1),
            ("atp", -3729.33, 0, 12, 2),
            ("adp", -1906.13, -3, 12, 0),
            ("adp", -1947.10, -2, 13, 0),
            ("adp", -2387.97, -1, 12, 1),
            ("adp", -2416.67, 0, 13, 1),
            ("adp", -1971.98, -1, 14, 0),
            ("amp", -1040.45, -2, 12, 0),
            ("amp", -1078.86, -1, 13, 0),
            ("amp", -1101.63, 0, 14, 0),
            ("amp", -1511.68, 0, 12, 1),
            ("pi", -1096.10, -2, 1, 0),
            ("pi", -1137.30, -1, 2, 0),
            ("pi", -1566.87, 0, 1, 1),
            ("h2o", -237.19, 0, 2, 0),
        ],
    )


@pytest.fixture(scope="module")
def reactant_dict(microspecies_df: pd.DataFrame) -> dict:
    """Create an ATP Compound object."""
    microspecies_df["ms_obj"] = [
        CompoundMicrospecies(
            id=row.Index,
            charge=row.z,
            number_protons=row.n_h,
            number_magnesiums=row.n_mg,
            ddg_over_rt=row.standard_dgf / (R * default_T).m_as("kJ/mol"),
        )
        for row in microspecies_df.itertuples(index=True)
    ]

    reactants = sorted(microspecies_df.reactant.unique())
    return {
        reactant: Compound(
            id=reactants.index(reactant), microspecies=df.ms_obj.tolist()
        )
        for reactant, df in microspecies_df.groupby("reactant")
    }


@pytest.fixture(scope="module")
def atp_hydrolysis_reaction(reactant_dict: dict) -> Reaction:
    return Reaction(
        {
            reactant_dict["atp"]: -1.0,
            reactant_dict["h2o"]: -1.0,
            reactant_dict["adp"]: 1.0,
            reactant_dict["pi"]: 1.0,
        }
    )


@pytest.mark.parametrize(
    ("p_h", "ionic_strength", "temperature", "p_mg", "expected_ddg"),
    [
        list(map(Q_, ["5.0", "0.25 M", "298.15K", "10.0", "-32.56 kJ/mol"])),
        list(map(Q_, ["7.0", "0.25 M", "298.15K", "10.0", "-36.02 kJ/mol"])),
        list(map(Q_, ["9.0", "0.25 M", "298.15K", "10.0", "-46.67 kJ/mol"])),
        list(map(Q_, ["5.0", "0.1 M", "298.15K", "3.0", "-32.22 kJ/mol"])),
        list(map(Q_, ["7.0", "0.1 M", "298.15K", "3.0", "-31.51 kJ/mol"])),
        list(map(Q_, ["9.0", "0.1 M", "298.15K", "3.0", "-41.60 kJ/mol"])),
        list(map(Q_, ["5.0", "0.25 M", "298.15K", "3.0", "-31.98 kJ/mol"])),
        list(map(Q_, ["7.0", "0.25 M", "298.15K", "3.0", "-32.43 kJ/mol"])),
        list(map(Q_, ["9.0", "0.25 M", "298.15K", "3.0", "-42.77 kJ/mol"])),
        list(map(Q_, ["5.0", "0.25 M", "350.00K", "3.0", "-31.62 kJ/mol"])),
        list(map(Q_, ["7.0", "0.25 M", "350.00K", "3.0", "-40.24 kJ/mol"])),
        list(map(Q_, ["9.0", "0.25 M", "350.00K", "3.0", "-53.53 kJ/mol"])),
    ],
)
def test_atp_hydrolysis(
    p_h, ionic_strength, temperature, p_mg, expected_ddg, atp_hydrolysis_reaction
):
    assert type(atp_hydrolysis_reaction) is Reaction

    ddg = atp_hydrolysis_reaction.transform(
        p_h=p_h,
        ionic_strength=ionic_strength,
        temperature=temperature,
        p_mg=p_mg,
    )
    assert ddg.m_as("kJ/mol") == pytest.approx(expected_ddg.m_as("kJ/mol"), abs=1e-2)

    # test the sensitivity_to_p_h() function
    sensitivity = atp_hydrolysis_reaction.sensitivity_to_p_h(
        p_h=p_h,
        ionic_strength=ionic_strength,
        temperature=temperature,
        p_mg=p_mg,
    )

    # calculate sensitivity "numerically"
    p_h1 = p_h - Q_(0.001)
    p_h2 = p_h + Q_(0.001)
    ddg1 = atp_hydrolysis_reaction.transform(
        p_h=p_h1,
        ionic_strength=ionic_strength,
        temperature=temperature,
        p_mg=p_mg,
    )
    ddg2 = atp_hydrolysis_reaction.transform(
        p_h=p_h2,
        ionic_strength=ionic_strength,
        temperature=temperature,
        p_mg=p_mg,
    )

    ddg_to_dph = (ddg2 - ddg1) / (p_h2 - p_h1)

    assert ddg_to_dph.m_as("kJ/mol") == pytest.approx(
        sensitivity.m_as("kJ/mol"), rel=1e-2
    )

    # test the sensitivity_to_I() function
    sensitivity = atp_hydrolysis_reaction.sensitivity_to_I(
        p_h=p_h,
        ionic_strength=ionic_strength,
        temperature=temperature,
        p_mg=p_mg,
    )

    # calculate sensitivity "numerically"
    ionic_strength1 = ionic_strength - Q_(0.001, "M")
    ionic_strength2 = ionic_strength + Q_(0.001, "M")
    ddg1 = atp_hydrolysis_reaction.transform(
        p_h=p_h,
        ionic_strength=ionic_strength1,
        temperature=temperature,
        p_mg=p_mg,
    )
    ddg2 = atp_hydrolysis_reaction.transform(
        p_h=p_h,
        ionic_strength=ionic_strength2,
        temperature=temperature,
        p_mg=p_mg,
    )

    ddg_to_dI = (ddg2 - ddg1) / (ionic_strength2 - ionic_strength1)

    assert ddg_to_dI.m_as("kJ/mol/molar") == pytest.approx(
        sensitivity.m_as("kJ/mol/molar"), rel=1e-2
    )
